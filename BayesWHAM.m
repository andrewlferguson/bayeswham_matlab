function BayesWHAM(dim,periodicity,T,harmonicBiasesFile,histBinEdgesFile,histDir,tol_WHAM,maxIter_WHAM,steps_MH,saveMod_MH,printMod_MH,maxDpStep_MH,seed_MH,prior,alpha)

% Copyright:	Andrew L. Ferguson, UIUC 
% Last updated:	2 Jan 2016

% SYNOPSIS
%
% code to compute dim-dimensional free energy surface from umbrella simulations by maximization of Bayes posterior, where posterior = likelihood * prior / evidence ~ likelihood * pior, plus estimate uncertainties by Metropolis-Hastings (MH) sampling from posterior  
%
% 1. assumes 	(i)   harmonic restraining potentials in dim-dimensional umbrella variables psi 
%            	(ii)  all simulations conducted at same temperature, T
%            	(iii) rectilinear binning of histograms collected over each umbrella simulation  
% 2. computes maximum a posteriori (MAP) estimate of unbiased probability distribution p(psi) by maximizing P(theta|data) = P(data|theta)*P(theta)/P(data), which is equivalent to solving WHAM equation with a Bayesian prior; 
%    with no prior (i.e., uniform prior) this is the maximum likelihood estimate, which is precisely equivalent to solving the WHAM equations  
% 3. estimates uncertainties by sampling from the posterior distribution P(theta|data) using Metropolis-Hastings algorithm 

% REFERENCES
%
% WHAM:				Kumar, S. et al., J. Comput. Chem. 13 8 1011-1021 (1992) 
%					Kumar, S. et al., J. Comput. Chem. 16 11 1339-1350 (1995) 
%					Ferrenberg, A.M. & Swendsen, R.H., Phys. Rev. Lett. 63 1195-1198 (1989) 
%					Roux, B., Comput. Phys. Comm. 91 275-282 (1995) 
% Bayes sampling:	Gallichio, E. et al., J. Phys. Chem. B 109 6722-6731 (2005)	
%					Bartels, C., Chem. Phys. Lett. 331 446-454 (2000) 
%					Zhu, F. & Hummer, G., J. Comput. Chem. 33 4 453-465 (2012) 
%					Habeck, M., Phys. Rev. Lett. 109 100601 (2012) 

% INPUTS
%
% dim                       - [int] dimensionality of umbrella sampling data in psi(1:dim) = number of coordinates in which umbrella sampling was conducted 
% periodicity               - [1 x dim bool] periodicity in each dimension across range of histogram bins 
%                             -> periodicity(i) == 0 => no periodicity in dimension i; periodicity(i) == 1 => periodicity in dimension i with period defined by range of histogram bins 
% T                         - [float] temperature in Kelvin at which all umbrella sampling simulations were conducted  
% harmonicBiasesFile        - [str] path to text file containing (1 + 2*dim) columns and S rows listing location and strength of biasing potentials in each of the S umbrella simulations 
%                             -> col 1 = umbrella simulation index 1..S, col 2:(2+dim-1) = biased simulation umbrella centers / (arbitrary units), col (2+dim):(2+2*dim-1) = harmonic restraining potential force constant / kJ/mol.(arbitrary units)^2 
% histBinEdgesFile          - [str] path to text file containing dim rows specifying edges of the rectilinear histogram bins in each dimension used to construct dim-dimensional histograms held in histDir/hist_i.txt 
%							  -> histBinEdgesFile contains k=1..dim lines each holding a row vector specifying the rectilinear histogram bin edges in dimension k 
%                             -> for M_k bins in dimension k, there are (M_k+1) edges 
%                             -> bins need not be regularly spaced
% histDir                   - [str] path to directory holding i=1..S dim-dimensional histograms in files hist_i.txt compiled from S biased trajectories over dim-dimensional rectilinear histogram grid specified in histBinEdgesFile  
%                             -> hist_i.txt comprises a row vector containing product_k=1..dim M_k = (M_1*M_2*...*M_dim) values recording histogram counts in each bin of the rectilinear histogram 
%                             -> values recorded in row major order (last index changes fastest) 
% tol_WHAM                  - [float] tolerance on maximum change in any element of p(psi) in self-consistent WHAM solution / - 
% maxIter_WHAM              - [int] maximum # steps allowed to converge to MAP probability estimate by direct iteration of WHAM equations 
% steps_MH                  - [int] # steps in Metropolis-Hastings sampling of Bayes posterior in p(psi) 
% saveMod_MH                - [int] Metropolis-Hastings save modulus 
% printMod_MH               - [int] Metropolis-Hastings print to screen modulus 
% maxDpStep_MH              - [float] maximum size of uniformly distributed step in p(psi) in randomly selected bin 
%                             -> tune to get acceptance probabilities of ~5-20% is a good rule of thumb 
% seed_MH					- [int] seed to initialize random number generator used to perform Metropolis-Hastings sampling of Bayes posterior
% prior                     - [str] type of prior to employ in Bayesian estimate of posterior probability distribution given umbrella sampling histograms
%                             -> prior = {'none','Dirichlet','Gaussian'} 
%                             -> N.B. WHAM equations are solved and prior applied to only the non-zero histogram bins (i.e., in case of Dirichlet pseudo-counts are only added to non-zero count histogram bins) 
% alpha                     - [float] hyperparameter of prior distribution
%                             -> prior = 'Dirichlet' : concentration parmeter for (symmetric) Dirichlet prior, equivalent to adding pseudo-counts of (alpha-1) to each histogram bin; alpha = 1 => uniform prior; alpha > 1 => favors dense + even posteriors, 0 < alpha < 1 => sparse + concentrated posteriors 
%                                        'Gaussian'  : scale parameter for Gaussian penalization of probabilities, exp(-alpha*p^2) 

% OUTPUTS
%
% hist_binCenters.txt       - [dim x M_k float] text file containing dim rows specifying centers of the rectilinear histogram bins in each dimension used to construct dim-dimensional histograms held in histDir/hist_i.txt 
%                             -> dimension k contains M_k bin centers 
%                             -> bins need not be regularly spaced
% hist_binWidths.txt        - [dim x M_k float] text file containing dim rows specifying widths of the rectilinear histogram bins in each dimension used to construct dim-dimensional histograms held in histDir/hist_i.txt 
%                             -> dimension k contains M_k bin widths 
%                             -> bins need not be regularly spaced
% p_MAP.txt                 - [1 x M float] text file containing MAP estimate of unbiased probability distribution p_l over l=1..M bins of dim-dimensional rectilinear histogram grid specified in histBinEdgesFile  
%                             -> values correspond to total probability residing within bin 
%                             -> values recorded in row major order (last index changes fastest) 
% pdf_MAP.txt               - [1 x M float] text file containing MAP estimate of unbiased probability density function pdf_l over l=1..M bins of dim-dimensional rectilinear histogram grid specified in histBinEdgesFile  
%                             -> values correspond to average probability density over the bin 
%                             -> values recorded in row major order (last index changes fastest) 
% betaF_MAP.txt             - [1 x M float] text file containing MAP estimate of unbiased free energy surface beta*F_l = -ln(p(psi)/binVolume) + const. over l=1..M bins of dim-dimensional rectilinear histogram grid specified in histBinEdgesFile to within an arbitrary additive constant 
%                             -> specified only up to an additive constant defined by shifting landscape to have zero mean; when plotting multiple free energy surfaces this is equivalent to minimizing the mean squared error between the landscapes 
%                             -> values recorded in row major order (last index changes fastest) 
% f_MAP.txt                 - [1 x S float] text file containing MAP estimates of f_i = Z/Z_i = ratio of unbiased partition function to that of biased simulation i, for i=1..S biased simulations 
% logL_MAP.txt              - [float] text file containing log of Bayes posterior up to an additive constant of the MAP estimate 
% p_MH.txt                  - [nSamples_MH x M float] text file containing nSamples_MH Metropolis-Hastings samples from the Bayes posterior of unbiased probability distribution p_l over l=1..M bins of dim-dimensional rectilinear histogram grid specified in histBinEdgesFile  
%                             -> values correspond to total probability residing within bin, NOT probability density
%                             -> values recorded in row major order (last index changes fastest) 
% pdf_MH.txt                - [nSamples_MH x M float] text file containing nSamples_MH Metropolis-Hastings samples from the Bayes posterior of unbiased probability density function pdf_l over l=1..M bins of dim-dimensional rectilinear histogram grid specified in histBinEdgesFile  
%                             -> values correspond to average probability density over the bin 
%                             -> values recorded in row major order (last index changes fastest) 
% betaF_MH.txt              - [nSamples_MH x M float] text file containing nSamples_MH Metropolis-Hastings samples from the Bayes posterior of unbiased free energy surface beta*F_l = -ln(p(psi)/binVolume) + const. over l=1..M bins of dim-dimensional rectilinear histogram grid specified in histBinEdgesFile to within an arbitrary additive constant 
%                             -> specified only up to an additive constant defined by shifting landscape to have zero mean; when plotting multiple free energy surfaces this is equivalent to minimizing the mean squared error between the landscapes 
%                             -> values recorded in row major order (last index changes fastest) 
% f_MH.txt                  - [nSamples_MH x S float] text file containing nSamples_MH Metropolis-Hastings samples from the Bayes posterior of f_i = Z/Z_i = ratio of unbiased partition function to that of biased simulation i, for i=1..S biased simulations 
% logL_MH.txt               - [nSamples_MH x 1 float] text file containing log of Bayes posterior up to an additive constant over the nSamples_MH Metropolis-Hastings samples from the posterior 
% step_MH.txt               - [nSamples_MH x 1 int] text file containing Metropolis-Hastings step associated with each of the nSamples_MH Metropolis-Hastings samples from the posterior 


%% parameters
kB = 0.0083144621;      % Boltzmann's constant / kJ/mol.K
beta = 1/(kB*T);        % beta = 1/kBT = (kJ/mol)^(-1)
genGaussPriorPower = 2;	% power of generalized Gaussian implemented for prior='Gaussian' (genGaussPriorPower=1 => Laplacian, genGaussPriorPower=2 => Gaussian, etc.)
                        % - hard coded as standard Gaussian prior with genGaussPriorPower = 2, but other values are possible
                        % - Laplacian prior has no impact (i.e., same as uniform prior) due to normalization of probability distribution since appears in log posterior as nothing more than an additive constant = -alpha  


%% printing inputs to screen
fprintf('\n')
fprintf('dim = %d\n',dim)
fprintf('periodicity = [%s]\n',int2str(periodicity))
fprintf('T = %e\n',T)
fprintf('harmonicBiasesFile = %s\n',harmonicBiasesFile)
fprintf('histBinEdgesFile = %s\n',histBinEdgesFile)
fprintf('histDir = %s\n',histDir)
fprintf('tol_WHAM = %e\n',tol_WHAM)
fprintf('maxIter_WHAM = %d\n',maxIter_WHAM)
fprintf('steps_MH = %d\n',steps_MH)
fprintf('saveMod_MH = %d\n',saveMod_MH)
fprintf('printMod_MH = %d\n',printMod_MH)
fprintf('maxDpStep_MH = %e\n',maxDpStep_MH)
fprintf('seed_MH = %d\n',seed_MH)
fprintf('prior = %s\n',prior)
fprintf('alpha = %e\n',alpha)
fprintf('\n')

%% checking inputs
if ~any(ismember({'none','Dirichlet','Gaussian'},prior))
    error('Input prior = %s unrecognized, must be one of {''none'',''Dirichlet'',''Gaussian''}');
end


%% loading data
fprintf('Loading data...\n')

% loading location and strength of harmonic biasing potentials in each of S umbrella simulations 
AA = importdata(harmonicBiasesFile);
S = size(AA,1);                             % # umbrella simualtions
if size(AA,2) ~= (1+2*dim)
    error('Number of columns in %s ~= 1+2*dim = %d as expected',harmonicBiasesFile,1+2*dim);
end
umbC = AA(:,2:1+dim);       % centers of S dim-dimensional harmonic umbrella potentials / (arbitrary units)
umbF = AA(:,2+dim:1+2*dim); % force constants of S umbrella harmonic umbrella potentials / kJ/mol.(arbitrary units)^2 
clear AA


% loading histogram bin edges 
binE = cell(dim,1);
fid = fopen(histBinEdgesFile,'rt');
for d = 1:dim
    tline = fgetl(fid);
    if ~ischar(tline)
        error('%s contains fewer than expected number of dim = %d lines specifying histogram bin edges',histBinEdgesFile,dim);
    end
    binE{d} = sscanf(tline,'%f')';
end
tline = fgetl(fid);
if ischar(tline)
    error('%s contains more than expected number of dim = %d lines specifying histogram bin edges',histBinEdgesFile,dim);
end
fclose(fid);

% - counting number of bins in each dimension M_k k=1..dim, and total number of bins M = product_k=1..dim M_k = M_1*M_2*...*M_dim 
M_k = nan(dim,1);
for d = 1:dim
    M_k(d) = length(binE{d})-1;
end
M = prod(M_k);

% - converting binEdges into binCenters and binWidths
binC = cell(dim,1);
binW = cell(dim,1);
for d = 1:dim
    binC{d} = 0.5 * ( binE{d}(1:end-1) + binE{d}(2:end) );
    binW{d} = binE{d}(2:end) - binE{d}(1:end-1);
end

% - computing period in each periodic dimension as histogram bin range 
period = nan(1,dim);
for d=1:dim
    if periodicity(d) ~= 0
        period(d) = binE{d}(end)-binE{d}(1);
    end
end


% loading histograms compiled over the S umbrella simulations recorded as row vectors in row major order (last index changes fastest) 
n_il = nan(S,M);
for i = 1:S
    AA = importdata([histDir,'/hist_',int2str(i),'.txt'],' ',0);
    if size(AA,1) ~= 1
        error('Did not find expected row vector in reading %s',[histDir,'/hist_',int2str(i),'.txt']);
    end
    if size(AA,2) ~= M
        error('Row vector in %s did not contain expected number of elements M = M_1*M_2*...*M_dim = %d given histogram bins specified in %s',[histDir,'/hist_',int2str(i),'.txt'],M,histBinEdgesFile);
    end
    n_il(i,:) = AA;
    clear AA
end


% precomputing aggregated statistics
N_i = sum(n_il,2);          % total counts in simulation i
M_l = sum(n_il,1)';         % total counts in bin l


fprintf('DONE!\n\n')


%% checking M_l for gaps 
% -> gaps in the aggregated M_l histogram (i.e., empty bins between populated bins) can cause numerical stability problems for the WHAM equations 
%    - conceptually, the presence of gaps indicates that there is not mutual (at least pairwise) overlap between all biased histograms, meaning that the estimate of the unbiased distribution may be disjoint since the biased distributions cannot be stitched together into a continuous probability distribution 
%    - mathematically, p_l becomes disjoint and cannot be consistently normalized across the full domain resulting in multiple disjoint distributions that leads to numerical instability in the WHAM equations 
% -> contiguity of the histogram (i.e., the absence of empty bins between populated bins) is a sufficient, but not necessary, condition to assure stability of the WHAM equations since this guarantees mutual overlap between all of the biased histograms and a non-disjoint estimate of the unbiased probability distribution (sufficiently small gaps are tolerable provided that the probability distribution does not become disjoint within machine precision) 

% computing adjacency matrix representing the graph specifying contiguity of populated hitogram bins 

    % TEST BLOCK for adjacency routines
    %
    %dim=3;
    %periodicity = [0,0,0];
    %
    %M_k = [3;4;2];
    %M = prod(M_k);
    %
    %A(:,:,1) = [
    %    1 1 1 0;
    %    1 1 0 0;
    %    0 0 0 1];
    %
    %A(:,:,2) = [
    %    1 1 0 1;
    %    1 1 0 1;
    %    0 1 0 1];
    %
    %M_l=[];
    %for i=1:M_k(1)
    %    for j=1:M_k(2)
    %        for k=1:M_k(3)
    %            M_l(end+1)=A(i,j,k);
    %        end
    %    end
    %end

fprintf('Checking connectivity of aggregated biased histogram...\n');
adjacency = zeros(M,M);
for l=1:M
    if M_l(l) > 0
        sub = ind2sub_RMO(M_k',l);
        for d=1:dim
            for q=[-1,+1]

                sub_d_plus_q = sub(d) + q;
                if sub_d_plus_q == M_k(d)+1
                    if periodicity(d) == 0
                        continue;
                    else
                        sub_d_plus_q = 1;          % wrapping through periodic boundary
                    end
                elseif sub_d_plus_q == 0
                    if periodicity(d) == 0
                        continue;
                    else
                        sub_d_plus_q = M_k(d);    % wrapping through periodic boundary
                    end
                end
                sub_neigh = sub;
                sub_neigh(d) = sub_d_plus_q;

                ind_neigh = sub2ind_RMO(M_k',sub_neigh);
                if M_l(ind_neigh) > 0
                    adjacency(l,ind_neigh) = 1;
                end
                
            end
        end
    end
end

% applying Tarjan's algorithm to identify contiguous regions of non-zero histogram bins within the adjacency matrix 
maskNZ = (M_l>0);
[nConComp,~] = graphconncomp(sparse(adjacency(maskNZ,maskNZ)));
if nConComp > 1
    fprintf('WARNING - aggregated biased histogram is disconnected, containing %d non-contiguous connected components\n',nConComp);
    fprintf('        - we might anticipate WHAM convergence difficulties\n');
    fprintf('        - consider making the aggregated histogram contiguous by increasing the bin size or conducting additional biased runs to patch the gaps\n');
    pause(1);
end
fprintf('DONE!\n\n')


%% precomputing biasing potentials for each simulation i in each bin l 
fprintf('Computing biasing potentials for each simulation in each bin...\n')
c_il = nan(S,M);            % S-by-M matrix containing biases due to artificial harmonic potential for simulation i in bin l
for i=1:S
    for l=1:M
        sub = ind2sub_RMO(M_k',l);
        expArg=0;
        for d=1:dim
            if periodicity(d) ~= 0
                delta = min( [ abs(binC{d}(sub(d))-umbC(i,d)), abs(binC{d}(sub(d))-umbC(i,d)+period(d)), abs(binC{d}(sub(d))-umbC(i,d)-period(d)) ] );
            else
                delta = abs(binC{d}(sub(d))-umbC(i,d));
            end
            expArg = expArg + 0.5*umbF(i,d)*delta^2;
        end
        c_il(i,l) = exp(-beta*expArg);
    end
    fprintf('\tProcessing of simulation %d of %d complete\n',i,S)
end
fprintf('DONE!\n\n')


%% MAP estimate of unbiased probability distribution over the umbrella histogram bins p_l by direct iteration of the WHAM equations
% -> analogy of WHAM equations under Bayesian prior derived using method of Lagrangian multipliers 
% -> Ref: C. Bartels "Analyzing biased Monte Carlo and molecular dynamics simulations" Chemical Physics Letters 331 446-454 (2000)

% - masking out all zero-count bins not visited by any simulation for computational efficiency 
%   -> optimal probability estimate for unvisited bins with M_l=0 is p_l=0, so can eliminate these bins from consideration 
%   -> Ref: C. Bartels "Analyzing biased Monte Carlo and molecular dynamics simulations" Chemical Physics Letters 331 446-454 (2000)
maskNZ = (M_l>0);
M_l_NZ = M_l(maskNZ);
c_il_NZ = c_il(:,maskNZ);
M_NZ = length(M_l_NZ);

% - initial guess for p_l and f_i
p_l_NZ = ones(length(M_l_NZ),1);
p_l_NZ = p_l_NZ./sum(p_l_NZ);
f_i = 1./(c_il_NZ*p_l_NZ);

% - direct iteration of WHAM equations to self-consistency at tolerance = tol 
dp_max = Inf;
dlogf_max = Inf;
iter = 1;
fprintf('Computing MAP estimate of probability distribution by direct iteration of the WHAM equations...\n');
while (dp_max > tol_WHAM)
    
    p_l_NZ_OLD = p_l_NZ;
    f_i_OLD = f_i;
    
    switch prior
        case 'Dirichlet'
            p_l_NZ = ( M_l_NZ + (alpha-1)*ones(size(M_l_NZ)) ) ./ ( (N_i.*f_i)'*c_il_NZ + M_NZ*(alpha-1)*ones(size(M_l_NZ))' )';
        case 'Gaussian'
            if alpha == 0   % no iterative solution required for alpha=0 => uniform prior => no prior
                p_l_NZ = M_l_NZ ./ ( (N_i.*f_i)'*c_il_NZ )';
            else
                a = genGaussPriorPower*alpha*ones(size(M_l_NZ));
                b = ((N_i.*f_i)'*c_il_NZ)';
                c = M_l_NZ;

                dp_max_inner = Inf;
                p_l_NZ_inner = p_l_NZ;
                while (dp_max_inner > tol_WHAM)     % iterative solution for regularized probability distribution under generalized Gaussian prior to same tolerance as WHAM equations 
                    p_l_NZ_inner_OLD = p_l_NZ_inner;
                    p_l_NZ_inner_POWt = p_l_NZ_inner.^genGaussPriorPower;
                    p_l_NZ_inner = (c - a.*p_l_NZ_inner_POWt ) ./ (b - sum(p_l_NZ_inner_POWt)*a);
                    dp_max_inner = max(abs(p_l_NZ_inner - p_l_NZ_inner_OLD));
                end
                p_l_NZ = p_l_NZ_inner;
                
            end
        otherwise
            p_l_NZ = M_l_NZ ./ ( (N_i.*f_i)'*c_il_NZ )';
    end
    p_l_NZ = p_l_NZ./sum(p_l_NZ);
    f_i = 1./(c_il_NZ*p_l_NZ);
    
    dp_max = max(abs(p_l_NZ - p_l_NZ_OLD));
        if ~isfinite(dp_max)
            error('ERROR - WHAM equation convergence failed');
        end
    dlogf_max = max(abs(log(f_i) - log(f_i_OLD)));
    
    fprintf('\tIteration %d, max(dp) = %15.5e\n',iter,dp_max);
    
    iter=iter+1;
    if iter > maxIter_WHAM
        error('ERROR - WHAM iteration did not converge within maxIter_WHAM = %d',maxIter_WHAM);
    end
    
end
fprintf('\tWHAM eqautions converged to within tolerance of %15.5e\n',tol_WHAM);
fprintf('DONE!\n\n')

% - unmasking p_l_NZ into p_l_MAP by reconstituting empty bins l with M_l=0 with optimal values p_l=0 
p_l_MAP = zeros(M,1);
p_l_MAP(maskNZ) = p_l_NZ;
f_i_MAP = f_i;

% - converting unbiased probability distribution estimate p_l into probability density function pdf_l and free energy estimate betaF_l 
%   -> mean zeroing betaF; when plotting multiple free energy surfaces this is equivalent to minimizing the mean squared error between the landscapes 
pdf_l_MAP = zeros(size(p_l_MAP));
betaF_l_MAP = nan(size(p_l_MAP));
for l=1:M
    sub = ind2sub_RMO(M_k',l);
    binVol=1;
    for d=1:dim
        binVol = binVol*binW{d}(sub(d));
    end
    if p_l_MAP(l) > 0
        pdf_l_MAP(l) = p_l_MAP(l)/binVol;
        betaF_l_MAP(l) = -log(p_l_MAP(l)/binVol);
    end
end
betaF_l_MAP = betaF_l_MAP - mean(betaF_l_MAP(isfinite(betaF_l_MAP)));

% - writing to file bin centers and bin widths of l=1..M bins of dim-dimensional rectilinear histogram grid as row vectors in row major order (last index changes fastest) 
fout = fopen('hist_binCenters.txt','wt');
    for d=1:dim
        fprintf(fout,'%15.5e',binC{d});
        fprintf(fout,'\n');
    end
fclose(fout);

fout = fopen('hist_binWidths.txt','wt');
    for d=1:dim
        fprintf(fout,'%15.5e',binW{d});
        fprintf(fout,'\n');
    end
fclose(fout);

% - writing to file MAP estimates for p_l, pdf_l, and betaF_l over l=1..M bins of dim-dimensional rectilinear histogram grid as row vectors in row major order (last index changes fastest) 
fout = fopen('p_MAP.txt','wt');
    fprintf(fout,'%15.5e',p_l_MAP);
    fprintf(fout,'\n');
fclose(fout);

fout = fopen('pdf_MAP.txt','wt');
    fprintf(fout,'%15.5e',pdf_l_MAP);
    fprintf(fout,'\n');
fclose(fout);

fout = fopen('betaF_MAP.txt','wt');
    fprintf(fout,'%15.5e',betaF_l_MAP);
    fprintf(fout,'\n');
fclose(fout);

% - writing to file MAP estimates for f_i = Z/Z_i = ratio of unbiased partition function to that of biased simulation i,for i=1..S biased simulations 
fout = fopen('f_MAP.txt','wt');
    fprintf(fout,'%15.5e',f_i_MAP);
    fprintf(fout,'\n');
fclose(fout);

% - writing to file log of Bayes posterior up to an additive constant for the MAP estimate 
fout = fopen('logL_MAP.txt','wt');
    maskNZ = (M_l>0);
    M_l_NZ = M_l(maskNZ);
    p_l_MAP_NZ = p_l_MAP(maskNZ);
    switch prior
        case 'Dirichlet'
            logL_MAP = N_i'*log(f_i) + ( M_l_NZ + (alpha-1)*ones(size(M_l_NZ)) )'*log(p_l_MAP_NZ);
        case 'Gaussian'
            logL_MAP = N_i'*log(f_i) + M_l_NZ'*log(p_l_MAP_NZ) - alpha*ones(size(M_l_NZ))'*(p_l_MAP_NZ.^genGaussPriorPower);
        otherwise
            logL_MAP = N_i'*log(f_i) + M_l_NZ'*log(p_l_MAP_NZ);
    end
    fprintf(fout,'%15.5e\n',logL_MAP);
fclose(fout);



%% Metropolis-Hastings sampling of Bayes posterior distribution to determine uncertainty in the MAP estimate of unbiased probability distribution 

% - masking out all zero-count bins not visited by any simulation for computational efficiency; 
%   accordingly perturbations considered only within bins visited by biased simulations 
%   -> optimal probability estimate for unvisited bins with M_l=0 is p_l=0, so can eliminate these bins from consideration 
%   -> Ref: C. Bartels "Analyzing biased Monte Carlo and molecular dynamics simulations" Chemical Physics Letters 331 446-454 (2000)

% - initializing rng
rng(seed_MH, 'twister')

% - performing steps_MH rounds of Metropolis-Hastings sampling
%   -> proposing trial moves as uniform random perturbations on [-maxDpStep_MH, maxDpStep_MH] of randomly selected bin of p_l and renormalizing; 
%      since trial moves are symmetric, this is actually Metropolis sampling which is a specialization of Metropolis-Hastings 
%   -> saving p_l, f_i, and log(L) (to within an additive constant) every saveMod_MH steps as samples from the posterior distribution
fprintf('Metropolis-Hastings sampling of Bayes posterior to determine uncertainty in MAP estimate of probability distribution...\n');

% - initializing sampling matrices
nSamples_MH = floor(steps_MH/saveMod_MH);
step_MH = zeros(nSamples_MH,1);
p_l_MH = zeros(M,nSamples_MH);
f_i_MH = nan(S,nSamples_MH);
logL_MH = nan(nSamples_MH,1);

% - commencing Metropolis-Hastings sampling
maskNZ = (M_l>0);
M_l_NZ = M_l(maskNZ);
c_il_NZ = c_il(:,maskNZ);
p_l_NZ = p_l_MAP(maskNZ);
M_NZ = length(p_l_NZ);
f_i = f_i_MAP;

hitCount=0;
for step=1:steps_MH
    
    % -- saving old state
    p_l_NZ_OLD = p_l_NZ;
    f_i_OLD = f_i;
    
    % -- proposing a (symmetric) move in {p_l_NZ,f_i}
    idx = randi(M_NZ);
    
    flag=1;
    p_l_NZ_idx_original = p_l_NZ(idx);
    while (flag)
        p_l_NZ(idx) = p_l_NZ(idx) + 2*(rand-0.5)*maxDpStep_MH;
        if ( p_l_NZ(idx)<0.0 || p_l_NZ(idx)>1.0 ) 
            p_l_NZ(idx) = p_l_NZ_idx_original;      % proposed move generates invalid p_l, try again
        else
            flag=0;                                 % proposed move generates valid p_l, proceed
        end
    end
    p_l_NZ = p_l_NZ./sum(p_l_NZ);
    f_i = 1./(c_il_NZ*p_l_NZ);
    
    % -- Metropolis accept/reject
    switch prior
        case 'Dirichlet'
            teller = exp( N_i'*log(f_i./f_i_OLD) + ( M_l_NZ + (alpha-1)*ones(size(M_l_NZ)) )'*log(p_l_NZ./p_l_NZ_OLD) );
        case 'Gaussian'
            teller = exp( N_i'*log(f_i./f_i_OLD) + M_l_NZ'*log(p_l_NZ./p_l_NZ_OLD) - alpha*ones(size(M_l_NZ))'*(p_l_NZ.^genGaussPriorPower - p_l_NZ_OLD.^genGaussPriorPower) );
        otherwise
            teller = exp( N_i'*log(f_i./f_i_OLD) + M_l_NZ'*log(p_l_NZ./p_l_NZ_OLD) );
    end
    if rand < teller            % accept
        hitCount=hitCount+1;
    else                        % reject
        p_l_NZ = p_l_NZ_OLD;
        f_i = f_i_OLD;
    end
    
    % -- saving sample to sampling arrays
    if ( mod(step,saveMod_MH) == 0 )
        jj = step/saveMod_MH;
        step_MH(jj) = step;
        p_l_MH(maskNZ,jj) = p_l_NZ;
        f_i_MH(:,jj) = f_i;
        switch prior
            case 'Dirichlet'
                logL_MH(jj) = N_i'*log(f_i) + ( M_l_NZ + (alpha-1)*ones(size(M_l_NZ)) )'*log(p_l_NZ);
            case 'Gaussian'
                logL_MH(jj) = N_i'*log(f_i) + M_l_NZ'*log(p_l_NZ) - alpha*ones(size(M_l_NZ))'*(p_l_NZ.^genGaussPriorPower);
            otherwise
                logL_MH(jj) = N_i'*log(f_i) + M_l_NZ'*log(p_l_NZ);
        end
    end
    
    % -- printing progress to screen
    if ( mod(step,printMod_MH) == 0 )
        fprintf('\tStep %d of %d, acceptance ratio = %15.5e\n',step,steps_MH,hitCount/step);
    end
    
end
fprintf('DONE!\n\n')

% - converting p_l_MH into pdf_l_MH and betaF_l_MH
%   -> mean zeroing betaF; when plotting multiple free energy surfaces this is equivalent to minimizing the mean squared error between the landscapes 
pdf_l_MH = zeros(size(p_l_MH));
betaF_l_MH = nan(size(p_l_MH));
for l=1:M
    sub = ind2sub_RMO(M_k',l);
    binVol=1;
    for d=1:dim
        binVol = binVol*binW{d}(sub(d));
    end
    for jj=1:size(p_l_MH,2)
        if p_l_MH(l,jj) > 0
            pdf_l_MH(l,jj) = p_l_MH(l,jj)/binVol;
            betaF_l_MH(l,jj) = -log(p_l_MH(l,jj)/binVol);
        end
    end
end
betaF_l_MH_colMean = nan(1,nSamples_MH);
for jj=1:nSamples_MH
    betaF_l_MH_col = betaF_l_MH(:,jj);
    betaF_l_MH_colMean(jj) = mean(betaF_l_MH_col(isfinite(betaF_l_MH_col)));
end
betaF_l_MH = betaF_l_MH - repmat( betaF_l_MH_colMean, size(betaF_l_MH,1), 1);

% - writing to file nSamples_MH Metropolis-Hastings samples from Bayes posterior of p_l, pdf_l, and betaF_l over l=1..M bins of dim-dimensional rectilinear histogram grid as row vectors in row major order (last index changes fastest) 
fout = fopen('p_MH.txt','wt');
    for jj=1:nSamples_MH
        fprintf(fout,'%15.5e',p_l_MH(:,jj));
        fprintf(fout,'\n');
    end
fclose(fout);

fout = fopen('pdf_MH.txt','wt');
    for jj=1:nSamples_MH
        fprintf(fout,'%15.5e',pdf_l_MH(:,jj));
        fprintf(fout,'\n');
    end
fclose(fout);

fout = fopen('betaF_MH.txt','wt');
    for jj=1:nSamples_MH
        fprintf(fout,'%15.5e',betaF_l_MH(:,jj));
        fprintf(fout,'\n');
    end
fclose(fout);

% - writing to file nSamples_MH Metropolis-Hastings samples from Bayes posterior of f_i = Z/Z_i = ratio of unbiased partition function to that of biased simulation i,for i=1..S biased simulations 
fout = fopen('f_MH.txt','wt');
    for jj=1:nSamples_MH
        fprintf(fout,'%15.5e',f_i_MH(:,jj));
        fprintf(fout,'\n');
    end
fclose(fout);

% - writing to file log of Bayes posterior up to an additive constant for the nSamples_MH Metropolis-Hastings samples from the posterior 
fout = fopen('logL_MH.txt','wt');
    fprintf(fout,'%15.5e\n',logL_MH);
fclose(fout);

% - writing to file associated MH step of the nSamples_MH Metropolis-Hastings samples from Bayes posterior  
fout = fopen('step_MH.txt','wt');
    fprintf(fout,'%15d\n',step_MH);
fclose(fout);








end











%---%
function sub = ind2sub_RMO(sz,ind)

% function to convert element index in a matrix of size sz to a subscripted location sub under row major ordering (RMO) 

% error checking
if size(sz,1) ~= 1
    error('Variable sz in ind2sub_RMO must be a row vector');
end
dim = size(sz,2);

ind_max = prod(sz);
if ( (ind < 1) || (ind > ind_max) )
    error('Variable ind = %d < 1 or ind = %d > # elements in tensor of size sz = %d in ind2sub_RMO',ind,ind,prod(sz));
end

% converting ind to sub
sub = nan(1,dim);
for ii=1:(dim-1)
    P=prod(sz(ii+1:end));
    sub_ii = ceil(ind/P);
    sub(ii) = sub_ii;
    ind = ind - (sub_ii-1)*P;
end
sub(end) = ind;

end

%---%
function ind = sub2ind_RMO(sz,sub)

% function to convert subscripted location sub in a matrix of size sz to element index under row major ordering (RMO) 

% error checking
if size(sz,1) ~= 1
    error('Variable sz in sub2ind_RMO must be a row vector');
end
dim = size(sz,2);

if size(sz,1) ~= 1
    error('Variable sub in sub2ind_RMO must be a row vector');
end

if size(sz,2) ~= size(sub,2)
    error('Variables sub and sz in sub2ind_RMO are not commensurate');
end

for ii=1:dim
    if ( sub(ii) < 1 || sub(ii) > sz(ii) )
        error('sub(%d) = %d < 1 or sub(%d) = %d > sz(%d) = %d in sub2ind_RMO',ii,sub(ii),ii,sub(ii),ii,sz(ii));
    end
end

% converting sub to ind
ind=0;
for ii=1:(dim-1)
    ind = ind + (sub(ii)-1)*prod(sz(ii+1:end));
end
ind = ind + sub(end);

end




